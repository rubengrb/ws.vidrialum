<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'BDVidria');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'nC.5w32]pcsJ<7n->a,7EX3*p*xb^B=z2Q~(nLc*]m!iU*Crl0BmLn/q07Cw5^yc');
define('SECURE_AUTH_KEY', 'J a;l7pkFBfngJi)&mv#O8}i~6JE+=!q~gvB&^FcmO]9Y|{0Zm1EFmY6n=JK.A@j');
define('LOGGED_IN_KEY', 'CGM!)X7^!PwoSh1,>B2.ZH_ZL|OF9^2F_c<:UY=h+njYQ{TQ*!,Muzr:ZG6|ccBU');
define('NONCE_KEY', 'e?%_dY)}F$Y2L8d1uGm=hY2f49n#4#kRovc5dw1xmqg8``IX~;Lcp3H,/G(2G,UX');
define('AUTH_SALT', 'cjwlSYd>*I~a=__oWP/6Ot]-)I|woQ[ I@:=VK0ju>p%U Z-<X[2.-4bG~[#KMOX');
define('SECURE_AUTH_SALT', 'hhYWM[)o;mcUeTASKV eNIJS/P/>5T@[k8Z@OFP;@3_]xh}2X{ZOc>AX40N2YTM&');
define('LOGGED_IN_SALT', 'f3RQ~y)7j}`tVN:TQqR;leG(E8b]J=au#&*:b7Nyp?)dBn3k|&/ca#Hy!Y?_6yn`');
define('NONCE_SALT', 'H-!$I]}:$?r-qNq@U$UD)!FZPZg{&g)S27jX90gFmyK%%,fZx!}7],zVRS9vu]z^');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'vm_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

